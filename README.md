# status_keeper
CIS510 SNAL

## Functionality

Accept HTTP request to store information and display on demand.

* update information: /updates/{NAME}/{0 or 1}
* breif report: /status 
* full report: /fullstatus
* save data: /save
* load data: /load
