import org.joda.time.DateTime;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static spark.Spark.get;

/**
 * Created by mingwei on 4/26/15.
 * <p/>
 * CIS510, System and Network Administration Labs (SNAL)
 * <p/>
 * This class is for keeping track of the connection status of the existing Pis
 */

public class Keeper implements Serializable {

    Map<String, List<Record>> records;
    Map<String, Stats> stats;
    DateTime startingPoint;
    String[] machines = {
            "duckpi01",
            "duckpi02",
            "duckpi03",
            "duckpi04",
            "duckpi05",
            "duckpi06",
            "duckpi07",
            "duckpi08"
    };

    public Keeper() {
        records = new HashMap<>();
        stats = new HashMap<>();
        for (String m : machines) {
            records.put(m, new ArrayList<>());
            stats.put(m, new Stats());
        }
        startingPoint = DateTime.now();
    }

    public static void main(String[] args) {
        Keeper keeper = new Keeper();

        keeper.initialServices();
    }

    public void initialServices() {
        get("/hello", (req, res) -> "Hello World");

        get("/update/:name/:status", (req, res) -> {
            String machineName = req.params(":name");
            Integer status = Integer.valueOf(req.params(":status"));

            if (!records.containsKey(machineName)) {
                return "Error: not defined machine name\n";
            }

            updateInfo(machineName, status);

            return String.format("Name: %s, status: %d\n", machineName, status);
        });

        get("/status", (request, response) -> {
            String output = "";
            output += "<pre>\n";
            output += "monitoring from time " + startingPoint.toString() + "\n\n";
            for (String m : machines) {
                String status;
                List<Record> rs = records.get(m);
                if (rs.size() != 0)
                    status = rs.get(rs.size() - 1).status == 0 ? "UP  " : "DOWN";
                else
                    status = "DOWN";
                output += String.format("%s %s, accessible percentage %.2f%% (%d/%d)\n", m, status, stats.get(m).percentage * 100, stats.get(m).connected, stats.get(m).total);
            }
            output += "</pre>\n";
            return output;
        });

        get("/fullstatus", (request, response) -> {
            String output = "";
            output += "<pre>\n";
            output += "monitoring from time " + startingPoint.toString() + "\n\n";
            for (String m : machines) {
                String status;
                List<Record> rs = records.get(m);
                if (rs.size() != 0)
                    status = rs.get(rs.size() - 1).status == 0 ? "UP  " : "DOWN";
                else
                    status = "DOWN";
                output += String.format("%s %s, accessible percentage %.2f%% (%d/%d)\n", m, status, stats.get(m).percentage * 100, stats.get(m).connected, stats.get(m).total);
                for (Record r : rs) {
                    output += String.format("\t%s : %d\n", r.time.toString(), r.status);
                }
                output += "\n";
            }
            output += "</pre>\n";
            return output;
        });

        get("/save", ((request, response) -> {
            save();
            return "done\n";
        }));

        get("/load", ((request, response) -> {
            load();
            return "done\n";
        }));
    }

    public void save() {
        try {

            FileOutputStream fout = new FileOutputStream("records.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(records);
            oos.close();

            fout = new FileOutputStream("stats.ser");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(stats);
            oos.close();
            System.out.println("Done");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void load() {
        try {

            FileInputStream fin = new FileInputStream("records.ser");
            ObjectInputStream ois = new ObjectInputStream(fin);
            records = (Map<String, List<Record>>) ois.readObject();
            ois.close();

            fin = new FileInputStream("stats.ser");
            ois = new ObjectInputStream(fin);
            stats = (Map<String, Stats>) ois.readObject();
            ois.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateInfo(String machine, Integer status) {
        records.get(machine).add(new Record(DateTime.now(), status));
        Stats s = stats.get(machine);
        s.total += 1;
        if (status == 0)
            s.connected++;
        s.percentage = s.connected / (double) s.total;
    }

    private class Record implements Serializable {
        public DateTime time;
        public int status; // 0-disconnected, 1-connected

        public Record(DateTime time, int status) {
            this.time = time;
            this.status = status;
        }
    }

    private class Stats implements Serializable {
        public int total;
        public int connected;
        public double percentage;

        public Stats() {
            total = 0;
            connected = 0;
            percentage = 0.0;
        }
    }
}
